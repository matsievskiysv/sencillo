package org.matsievskiysv.pagerenderview

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint

import android.util.Log

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.Lifecycle

import java.nio.ByteBuffer
import java.nio.ByteOrder

import kotlin.math.*

import kotlinx.coroutines.launch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Job
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive

import com.glidebitmappool.GlideBitmapPool

import org.matsievskiysv.pagerenderview.geometry.PointI
import org.matsievskiysv.pagerenderview.geometry.RectI
import org.matsievskiysv.pagerenderview.geometry.RectF
import org.matsievskiysv.pagerenderview.interfaces.OverlayDrawable
import org.matsievskiysv.pagerenderview.interfaces.Document
import org.matsievskiysv.pagerenderview.interfaces.Page
import org.matsievskiysv.pagerenderview.helpers.*

/**
 * Load page tiles.
 */
class PageLoader() : LifecycleObserver {
    private val logName = this::class.simpleName
    /**
     * Debug flag. Disable tile draw
     */
    private val DISABLE_TILE_DRAW = false
    /**
     * Debug flag. Add async page loading delay
     */
    private val PAGE_LOAD_DELAY = false
    /**
     * Document handle
     */
    public var document: Document? = null
    /**
     * Life cycle object
     */
    public var lifeCycle: Lifecycle? = null
        set(value) {
            field = value
            value!!.addObserver(this)
        }
    /**
     * Draw page overlay images on bitmap.
     * @param bitmap Bitmap to put image into
     * @param pageNum Page number
     * @param tileLeft Cropping boundary
     * @param tileTop Cropping boundary
     * @param tileRight Cropping boundary
     * @param tileBottom Cropping boundary
     */
    lateinit public var overlayDraw: (Bitmap, Int, Float, Float, Float, Float) -> Unit
    /**
     * Document handle
     */
    public var bitmapCacheSize = 128L * 1024L * 1024L
        set(value) {
            field = value
            invalidate()
            GlideBitmapPool.initialize(value.toInt())
        }
    /**
     * Coroutine scope
     */
    public var coroutineScope : CoroutineScope? = null
    /**
     * Render request callback
     */
    public var renderRequest: () -> Unit = {}
    /**
     * Pages cache
     */
    private var pageCache: PageCache? = null
    /**
     * Cache for bitmaps
     */
    private val bitmapCache =
        LRUCache<BitmapId, BitmapData>(bitmapCacheSize, destructor = {
            id, p ->
                p.job?.cancel()
                p.bitmap?.let { Log.v(logName, "free bitmap ${id}"); GlideBitmapPool.putBitmap(it) }
                bitmapDataPool.release(p)
                bitmapIdPool.release(id)
        })
    /**
     * Pool for render job objects.
     */
    private val bitmapDataPool = Pool(30, true) { BitmapData(null, null) }
    /**
     * Pool for bitmap ID opjects.
     */
    private val bitmapIdPool = Pool(30, true) { BitmapId(0, 0, 0, 0, 0, 0) }
    /**
     * Size of bitmap used for cropping calculations.
     */
    private val cropBitmapSize = 1000
    /**
     * Defer page render.
     */
    public var defer = false
    /**
     * Execute function on start.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onStart() {
        Log.v(logName, "onStart hook")
        GlideBitmapPool.initialize(bitmapCacheSize.toInt())
    }
    /**
     * Execute function on stop.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun onStop() {
        Log.v(logName, "onStop hook")
        Log.v(logName, "stop crop job")
        bgJob?.cancel()
        document?.withBlock {
            // wait for spawned processes to exit critical section
            Log.v(logName, "clear bitmap cache")
            bitmapCache.clear()
        }
        GlideBitmapPool.shutDown()
        Log.v(logName, "clear page cache")
        pageCache?.clear()
    }
    /**
     * On invalidate change hook
     */
    public var onInvalidate: () -> Unit = {}
    /**
     * Initialize object with document handle.
     * @param doc Document handle
     */
    public fun initialize(doc: Document) {
        document = doc
        pageCache = PageCache(doc)
    }
    /**
     * Clear all caches
     */
    public fun invalidate() {
        document?.withBlock {
            // wait for spawned processes to exit critical section
            Log.v(logName, "clear bitmap cache")
            bitmapCache.clear()
        }
        onInvalidate()
    }

    private fun getBitmapId(tile: PageLayoutManager.PageTile, scaleCode: Int): BitmapId =
        bitmapIdPool.acquire()!!.apply {
            scale = scaleCode
            page = tile.page
            tileX = tile.tile.x
            tileY = tile.tile.y
            pageWidth = ceil(cropBitmapSize * (tile.crop.right - tile.crop.left)).toInt()
            pageHeight = ceil(cropBitmapSize * (tile.crop.bottom - tile.crop.top)).toInt()
        }
    /**
     * Load tile bitmap
     * @param tile Page tile
     * @param tileScale Page scale
     * @param tileScaleId Page scale ID
     */
    public fun loadTileBitmap(tile: PageLayoutManager.PageTile, tileScale: Float, tileScaleId: Int) {
        val id = getBitmapId(tile, tileScaleId)
        Log.v(logName, "search tile bitmap ${tile} with ID ${id}")
        if (!bitmapCache.exists(id)) {
            val newBitmap = GlideBitmapPool.getBitmap(
                ceil(tile.position.width() * tileScale).toInt(),
                ceil(tile.position.height() * tileScale).toInt(),
                Bitmap.Config.ARGB_8888
            )
            val entry = bitmapDataPool.acquire()!!
            Log.d(logName, "submit rendering of ${tile}")
            val job = renderTile(
                newBitmap,
                tile.page,
                tile.crop.left,
                tile.crop.top,
                tile.crop.right,
                tile.crop.bottom,
            )
            bitmapCache.push(id, entry.also { it.job = job; it.bitmap = newBitmap },
                             size = newBitmap.getAllocationByteCount())
        } else {
            bitmapIdPool.release(id)
        }
    }
    /**
     * Render tile.
     * @param bitmap Bitmap to put image into
     * @param pageNum Page number
     * @param tileLeft Cropping boundary
     * @param tileTop Cropping boundary
     * @param tileRight Cropping boundary
     * @param tileBottom Cropping boundary
     */
    private fun renderTile(
        bitmap: Bitmap,
        pageNum: Int,
        tileLeft: Float,
        tileTop: Float,
        tileRight: Float,
        tileBottom: Float,
    ): Job =
        coroutineScope!!.launch(
            Dispatchers.Default,
            start = when (defer) {
                true -> CoroutineStart.LAZY
                false -> CoroutineStart.DEFAULT
            }
        ) {
            if (isActive) {
                pageCache!!.withPage(pageNum) {
                    page ->
                        if (PAGE_LOAD_DELAY) {
                            Thread.sleep((500..1500).random().toLong())
                        }
                    if (isActive) {
                        page.draw(bitmap, tileLeft, tileTop, tileRight, tileBottom)
                    }
                }
                // TODO: add bitmap image filtering here
                overlayDraw(bitmap, pageNum, tileLeft, tileTop, tileRight, tileBottom)
                if (isActive) {
                    renderRequest()
                }
            }
        }
    /**
     * Start deferred render of functions
     * @param tile Page tile
     * @param scaleId Page scale ID
     */
    public fun startDeferredRender(tile: PageLayoutManager.PageTile, scaleId: Int) {
        val id = getBitmapId(tile, scaleId)
        if (bitmapCache.exists(id)) {
            val job = bitmapCache.get(id)?.job!!
            if (!job.isActive || !job.isCompleted || !job.isCancelled) {
                job.start()
            }
        }
        bitmapIdPool.release(id)
    }
    /**
     * Get tile bitmap
     * @param tile Page tile
     * @param scaleId Page scale ID
     * @return Bitmap
     */
    public fun getTile(tile: PageLayoutManager.PageTile, scaleId: Int): Bitmap? {
        val id = getBitmapId(tile, scaleId)
        val bitmapData = bitmapCache.get(id)
        bitmapIdPool.release(id)
        if (bitmapData != null && bitmapData.job!!.isCompleted && !bitmapData.job!!.isCancelled) {
            return bitmapData.bitmap
        } else {
            return null
        }
    }
    /**
     * Get bitmap from pool
     * @param width Bitmap width
     * @param height Bitmap height
     * @return Bitmap
     */
    public fun getBitmap(width: Int, height: Int): Bitmap =
        GlideBitmapPool.getBitmap(width, height, Bitmap.Config.ARGB_8888)
    /**
     * Return bitmap to pool
     * @param bitmap Bitmap
     */
    public fun putBitmap(bitmap: Bitmap) = GlideBitmapPool.putBitmap(bitmap)

    private var bgJob: Job? = null
    /**
     * Calculate crop boundaries
     * @param pageList Page list to write crop data
     * @param callback Callback to call after job is done
     * @return Crop job
     */
    public fun calculateCrop(pageList: List<PageLayoutManager.PageGeometry>, callback: () -> Unit) {
        val bitmapSize = PointI(cropBitmapSize, cropBitmapSize)
        val cropBitmap = getBitmap(bitmapSize.x, bitmapSize.y)
        val bitmapBuffer = ByteBuffer.allocateDirect(4 * 4 * bitmapSize.x * bitmapSize.y)
        bitmapBuffer.order(ByteOrder.nativeOrder()).rewind()
        val cropPixels = RectI()
        bgJob = coroutineScope!!.launch(Dispatchers.Default) {
            for (page in 0 until pageList.size) {
                if (!isActive) {
                    putBitmap(cropBitmap)
                    break
                }
                Log.v(logName, "crop page ${page}")
                pageCache!!.withPage(page) { it.draw(cropBitmap, 0f, 0f, 1f, 1f) }
                cropBitmap.copyPixelsToBuffer(bitmapBuffer)
                bitmapBuffer.rewind()
                getCropBounds(cropPixels, bitmapSize, bitmapBuffer)
                pageList[page].crop.iSet(cropPixels.left.toFloat() / bitmapSize.x,
                                         cropPixels.top.toFloat() / bitmapSize.y,
                                         1f - cropPixels.right.toFloat() / bitmapSize.x,
                                         1f - cropPixels.bottom.toFloat() / bitmapSize.y)
            }
            putBitmap(cropBitmap)
        }
        bgJob?.invokeOnCompletion{
            cause -> when (cause) {
                null -> {
                    Log.v(logName, "run crop job callback")
                    callback()
                }
                else -> {
                    Log.v(logName, "crop job stopped ${cause}")
                }
            }
        }
    }

    // {{{ aux classes
    /**
     * Bitmap identifier
     * @property scale Integer scale code
     * @property page Page number
     * @property tileX Horizontal tile number
     * @property tileY Vertical tile number
     * @property pageWidth Tile relative width
     * @property pageHeight Tile relative height
     */
    data class BitmapId(
        var scale: Int,
        var page: Int,
        var tileX: Int,
        var tileY: Int,
        var pageWidth: Int,
        var pageHeight: Int,
    )
    /**
     * Bitmap container for job queue
     * @param job Render job
     * @param bitmap Bitmap
     */
    data class BitmapData(
        var job: Job?,
        var bitmap: Bitmap?,
    )
    // }}}
    /**
     * Calculate page crop boundaries
     * @param size Bitmap size
     * @param pixels Bitmap buffer
     * @param bounds Rectangle to put calculation results into
     */
    private external fun getCropBounds(bounds: RectI, size: PointI, pixels: ByteBuffer)
    /**
     * Configure cropping. FIXME: check implementation
     */
    private external fun cropConfigure(bgColor: Int, bgColorTolerance: Int, margins: RectI)

    /**
     * C functions
     */
    companion object {
        init {
            System.loadLibrary("pagerenderview")
        }
    }

    /**
     * Cache document page handlers for faster processing
     * @property document Document handle
     * @param capacity Capacity of the cache
     * @param cache Page cache
     */
    class PageCache(private val document: Document, capacity: Long = 20) {
        private val logName = this::class.simpleName
        /**
         * Page LRU cache
         */
        public val cache = LRUCache<Int, Page>(
            capacity,
            destructor = { num, page -> page.withBlock{ Log.v(logName, "free page ${num}"); page.close() } }
        )
        /**
         * Execute function on page handle
         * @param pageNum Page number
         * @param func Function to execute. Function argument is [Page]
         */
        suspend fun <R> withPage(pageNum: Int, func: (Page) -> R) {
            document.withLock {
                val page = cache.get(pageNum) ?: it.page(pageNum).also { cache.push(pageNum, it) }
                page.withBlock(func)
            }
        }
        /**
         * Clear page cache and release all underlying page handles
         */
        fun clear() = document.withBlock { cache.clear() }
    }
}
