package org.matsievskiysv.pagerenderview

import android.util.Log
import android.util.Size

import android.os.Parcel
import android.os.Parcelable

import kotlin.math.*

import org.matsievskiysv.pagerenderview.geometry.*
import org.matsievskiysv.pagerenderview.parcel.PageLayoutManagerParcel
import org.matsievskiysv.pagerenderview.helpers.Grid
import org.matsievskiysv.pagerenderview.helpers.Pool
import org.matsievskiysv.pagerenderview.helpers.RecycleList


/**
 * Direction enum
 */
enum class Direction {
    /**
     * Horizontal layout
     */
    HORIZONTAL,
    /**
     * Vertical layout
     */
    VERTICAL
}

/**
 * Manage document pages layout and calculate page positions.
 * This class calculates page positions according to the document layout.
 * For the given [viewPort] class calculates visible pages and page tiles.
 * This class operates exclusively with real page sizes.
 * Page coordinates are integer values, floating point calculations are rounded.
 */
class PageLayoutManager() {
    private val logName = this::class.simpleName
    // {{{ Page layout
    /**
     * Number of pages in document
     */
    private var pageNum = 0
    /**
     * List of page properties of type [PageGeometry].
     * This
     */
    lateinit public var pageGeometries: List<PageLayoutManager.PageGeometry>
    /**
     * Determine in which direction next page will be placed
     */
    public var pageFlow = Direction.VERTICAL
        set(value) {
            Log.v(logName, "set flow ${value}")
            arrangeDirtyFlag = true
            field = value
        }
    /**
     * Determine page grid resize direction.
     * If value is [Direction.VERTICAL], column number will be constant and row number will be calculated from the
     * page number and visa versa.
     */
    public var gridDirection = Direction.VERTICAL
        set(value) {
            Log.v(logName, "set direction ${value}")
            arrangeDirtyFlag = true
            field = value
        }
    /**
     * Determine horizontal page layout
     */
    public var leftToRight = true
        set(value) {
            Log.v(logName, "set horizontal layout ${value}")
            arrangeDirtyFlag = true
            field = value
        }
    /**
     * Determine vertical page layout
     */
    public var topToBottom = true
        set(value) {
            Log.v(logName, "set vertical layout ${value}")
            arrangeDirtyFlag = true
            field = value
        }
    /**
     * Determine page grid number of columns.
     * Ignored if gridDirection is [Direction.HORIZONTAL]
     */
    public var columns = 1
        set(value) {
            Log.v(logName, "set columns ${value}")
            arrangeDirtyFlag = true
            field = value
        }
    /**
     * Determine page grid number of rows.
     * Ignored if gridDirection is [Direction.VERTICAL]
     */
    public var rows = 1
        set(value) {
            Log.v(logName, "set rows ${value}")
            arrangeDirtyFlag = true
            field = value
        }
    /**
     * Add space around pages
     */
    public var padding = RectI(0, 0, 0, 0)
        set(value) {
            Log.v(logName, "set padding ${value}")
            arrangeDirtyFlag = true
            field = value
        }
    /**
     * Number of tiles per page
     */
    public var tiles = PointI(1, 1)
    /**
     * Enable or disable page crop
     */
    public var crop = false
        set(value) {
            Log.v(logName, "set crop ${value}")
            if (value) {
                normalizeCrop()
            }
            arrangeDirtyFlag = true
            field = value
        }
    // }}}

    // {{{ Interfaces
    /**
     * This rectangle represents visible part of the document. This object must not be dereferenced.
     */
    public var viewPort = RectI()
    /**
     * Document rectangle. This object must not be dereferenced.
     */
    public var docRect = RectI()
    /**
     * Rectangle of currently visible tiles.
     * This rectangle enlarges [viewPort] area to include all tiles that are partially visible.
     */
    public var visibleRect = RectI()
    /**
     * List of page tiles, visible in [viewPort].
     * [RecycleList] provides a growable pool of [PageTile] objects.
     */
    public var pageTiles =
            RecycleList(15) {
                PageTile(0, RectI(), RectF(), PointI(), PointI())
            }
    // }}}

    // {{{ Internal
    /**
     * Flag indicating that document layout has changed
     */
    private var arrangeDirtyFlag = true
    /**
     * Flag indicating all pages have the same size.
     * If true, simpler and faster calculation algorithms will be used.
     */
    private var evenPages = false
    /**
     * Flag indicating all pages have the same height.
     * If true, simpler and faster calculation algorithms will be used.
     */
    private var evenRows = false
    /**
     * List holding vertical coordinates of page separation lines.
     * If [evenRows] is true, this list holds only one value of row hight.
     */
    private var coordsRow = List(rows) { 0 }
    /**
     * Maximum row size
     */
    public var maxRow = 0
    /**
     * Flag indicating all pages have the same width.
     * If true, simpler and faster calculation algorithms will be used.
     */
    private var evenColumns = false
    /**
     * List holding horizontal coordinates of page separation lines.
     * If [evenColumns] is true, this list holds only one value of column width.
     */
    private var coordsCol = List(columns) { 0 }
    /**
     * Maximum column size
     */
    public var maxCol = 0
    /**
     * Page indexes organized in a grid
     */
    private val pageGrid = Grid<Int>(pageNum, 1)

    private val pointIPool = Pool(3, ::PointI)
    private val rectIPool = Pool(6, ::RectI)
    private val rectFPool = Pool(3, ::RectF)

    // }}}
    /**
     * Adjust [viewPort] position.
     * Move [viewPort] into the [docRect] and if it doesn't fit, center it.
     */
    public fun moveIntoDoc() {
        Log.v(logName, "move into doc")
        try {
            viewPort.iMoveIntoHorizontal(docRect)
        } catch (_: IllegalArgumentException) {
            Log.d(logName, "horizontal overflow")
            val offset = (docRect.width() - viewPort.width()) / 2
            val width = viewPort.width()
            viewPort.left = offset
            viewPort.right = offset + width
        }
        try {
            viewPort.iMoveIntoVertical(docRect)
        } catch (_: IllegalArgumentException) {
            Log.d(logName, "vertical overflow")
            val offset = (docRect.height() - viewPort.height()) / 2
            val height = viewPort.height()
            viewPort.top = offset
            viewPort.bottom = offset + height
        }
    }
    /**
     * Jump to the specified point on the page
     * @param page Page number
     * @param horizontal Horizontal coordinate of the [viewPort] top left corner
     * @param vertical Vertical coordinate of the [viewPort] top left corner
     * @param side Which rectangle point to move to point
     */
    public fun moveToPage(page: Int, horizontal: Int, vertical: Int) {
        arrange()
        val (row, col) = pageGrid.findFirst(clipnum(page, 0, pageNum-1))
        val pagePos = pointIPool.acquire()!!
        val pos = pointIPool.acquire()!!.apply { x = horizontal; y = vertical }
        if (evenRows) {
            pagePos.y = coordsRow[0] * row
        } else {
            pagePos.y = coordsRow[row]
        }
        if (evenColumns) {
            pagePos.x = coordsCol[0] * col
        } else {
            pagePos.x = coordsCol[col]
        }
        val viewAncor = pointIPool.acquire()!!
        viewPort.pointInto(
            if (leftToRight) {
                if (topToBottom) {
                    RectSide.TOPLEFT
                } else {
                    RectSide.BOTTOMLEFT
                }
            } else {
                if (topToBottom) {
                    RectSide.TOPRIGHT
                } else {
                    RectSide.BOTTOMRIGHT
                }
            }, viewAncor)
        viewPort.minusAssign(viewAncor)
        viewPort.plusAssign(pagePos)
        viewPort.plusAssign(pos)
        pointIPool.release(viewAncor)
        pointIPool.release(pagePos)
        pointIPool.release(pos)
    }
    /**
     * Jump to the specified point on the page
     * @param page Page number
     * @param pos Coordinate of the [viewPort] top left corner
     */
    public fun moveToPage(page: Int, pos: PointI) = moveToPage(page, pos.x, pos.y)
    /**
     * Jump to the top left point on the page
     * @param page Page number
     */
    public fun moveToPage(page: Int) = moveToPage(page, 0, 0)
    /**
     * Jump to the specified point on the page
     * @param page Page number
     * @param pos Normalized coordinate of the [viewPort] top left corner
     */
    public fun moveToPage(page: Int, pos: PointF) = moveToPage(
        page,
        round(pageGeometries[page].size.x * clipnum(pos.x, 0f, 1f)).toInt(),
        round(pageGeometries[page].size.y * clipnum(pos.y, 0f, 1f)).toInt()
    )
    /**
     * Convert point from document coordinate system to page coordinate system i.e. translate point to position on page.
     * @param point Coordinate of point. Page coordinate will be set in this object in place if there's page at point.
     * Otherwise it will be set to zeros.
     * @return Page at point or null.
     */
    public fun getPositionOnPage(point: PointI): Int? {
        arrange()

        var row: Int = 0
        var rowPos: Int
        var col: Int = 0
        var colPos: Int
        if (evenRows) {
            row = point.y / coordsRow[0]
            rowPos = row * coordsRow[0]
        } else {
            while (coordsRow[row] < point.y) {
                row++
            }
            row--
            rowPos = coordsRow[row]
        }
        if (evenColumns) {
            col = point.x / coordsCol[0]
            colPos = col * coordsCol[0]
        } else {
            while (coordsCol[col] < point.y) {
                col++
            }
            col--
            colPos = coordsCol[col]
        }
        val pageNum = pageGrid[row, col] ?: return null
        val page = pageGeometries[pageNum]
        val pageSize = pointIPool.acquire()!!.apply {
            x = if (crop) {
                round(page.size.x * (page.crop.right - page.crop.left)).toInt()
            } else {
                page.size.x
            }
            y = if (crop) {
                round(page.size.y * (page.crop.bottom - page.crop.top)).toInt()
            } else {
                page.size.y
            }
        }
        val pageTopLeft = pointIPool.acquire()!!.apply {
            x = if (crop) {
                round(page.size.x * page.crop.left).toInt()
            } else {
                0
            }
            y = if (crop) {
                round(page.size.y * page.crop.top).toInt()
            } else {
                0
            }
        }
        point.iSet(point.x - colPos - padding.left - pageTopLeft.x,
                   point.y - rowPos - padding.top - pageTopLeft.y)
        return if (point.x < 0 || point.x > pageSize.x ||
                   point.y < 0 || point.y > pageSize.y) {
            point.iSet(0, 0)
            pointIPool.release(pageSize)
            pointIPool.release(pageTopLeft)
            null
        } else {
            pointIPool.release(pageSize)
            pointIPool.release(pageTopLeft)
            pageNum
        }
    }
    /**
     * Calculate page location of the [pageView] center
     * @return page location of the [pageView] center
     */
    public fun getCurrentPage(): Int {
        arrange()

        val pos = pointIPool.acquire()!!
        viewPort.topLeftInto(pos)
        pos.x += viewPort.width() / 2
        pos.y += viewPort.height() / 2
        var row = 0
        var col = 0
        if (evenRows) {
            row = pos.y / coordsRow[0]
        } else {
            while (coordsRow[row] < pos.y) {
                row++
            }
            row--
        }
        if (evenColumns) {
            col = pos.x / coordsCol[0]
        } else {
            while (coordsCol[col] < pos.y) {
                col++
            }
            col--
        }
        pointIPool.release(pos)
        return pageGrid[row, col] ?: pageNum
    }
    /**
     * Calculate page location
     * @param page Page number
     * @param container Container for page location
     */
    public fun pageLocation(page: Int, container: PointI) {
        arrange()

        val (row, col) = pageGrid.findFirst(page)
        container.y = if (evenRows) {
            row * coordsRow[0]
        } else {
            coordsRow[row]
        }
        container.x = if (evenColumns) {
            col * coordsCol[0]
        } else {
            coordsCol[col]
        }
        Log.d(logName, "calculated page location ${page} ${container}")
    }
    /**
     * Lazily arrange pages on the grid
     */
    public fun arrange() {
        if (!arrangeDirtyFlag) {
            return
        }
        if (pageNum < 1) {
            throw IndexOutOfBoundsException()
        }
        Log.v(logName, "arranging")
        when (gridDirection) {
            Direction.VERTICAL -> {
                if (columns > pageNum) {
                    columns = pageNum
                    Log.w(logName, "reducing number of columns to ${columns}")
                }
                rows = ceil(pageNum.toFloat() / columns).toInt()
            }
            Direction.HORIZONTAL -> {
                if (rows > pageNum) {
                    rows = pageNum
                    Log.w(logName, "reducing number of rows to ${rows}")
                }
                columns = ceil(pageNum.toFloat() / rows).toInt()
            }
        }
        lateinit var horizontal: IntProgression
        lateinit var vertical: IntProgression
        when (leftToRight) {
            true -> horizontal = 0 until columns
            false -> horizontal = columns - 1 downTo 0
        }
        when (topToBottom) {
            true -> vertical = 0 until rows
            false -> vertical = rows - 1 downTo 0
        }
        // set pages' positions on the grid
        var page = 0
        pageGrid.clear()
        pageGrid.reshape(rows, columns)
        when (pageFlow) {
            Direction.VERTICAL -> {
                loop@ for (j in horizontal) {
                    for (i in vertical) {
                        pageGrid[i, j] = page
                        if (page < pageNum - 1) {
                            page++
                        } else {
                            break@loop
                        }
                    }
                }
            }
            Direction.HORIZONTAL -> {
                loop@ for (i in vertical) {
                    for (j in horizontal) {
                        pageGrid[i, j] = page
                        if (page < pageNum - 1) {
                            page++
                        } else {
                            break@loop
                        }
                    }
                }
            }
        }
        Log.v(logName, "simplified arranging algorithm ${evenPages && !crop}")
        // calculate row and column sizes
        if (evenPages && !crop) {
            // simplified algorithm if pages are even
            evenRows = true
            coordsRow = listOf(pageGeometries[0].size.y + padding.top + padding.bottom)
            maxRow = coordsRow[0]
            evenColumns = true
            coordsCol = listOf(pageGeometries[0].size.x + padding.left + padding.right)
            maxCol = coordsCol[0]
            val (rows, columns) = pageGrid.dims
            docRect.iSet(0, 0, coordsCol[0] * columns, coordsRow[0] * rows)
        } else {
            coordsRow =
                    (0 until rows).map {
                        (0 until columns)
                                .map { i -> pageGrid[it, i] }
                                .filterNotNull()
                                .map {
                                    if (crop) {
                                        round(pageGeometries[it].size.y * pageGeometries[it].crop.height()).toInt()
                                    } else {
                                        pageGeometries[it].size.y
                                    } + padding.top + padding.bottom
                                }
                                .reduce { acc, p -> max(acc, p) }
                    }
            evenRows = coordsRow.all { x -> x == coordsRow[0] }
            maxRow = coordsRow.maxOrNull()!!
            coordsRow = coordsRow.runningFold(0) { acc, x -> acc + x }
            coordsCol =
                    (0 until columns).map {
                        (0 until rows)
                                .map { i -> pageGrid[i, it] }
                                .filterNotNull()
                                .map {
                                    if (crop) {
                                        round(pageGeometries[it].size.x * pageGeometries[it].crop.width()).toInt()
                                    } else {
                                        pageGeometries[it].size.x
                                    } + padding.left + padding.right
                                }
                                .reduce { acc, p -> max(acc, p) }
                    }
            evenColumns = coordsCol.all { x -> x == coordsCol[0] }
            maxCol = coordsCol.maxOrNull()!!
            coordsCol = coordsCol.runningFold(0) { acc, x -> acc + x }
            docRect.iSet(0, 0, coordsCol.last(), coordsRow.last())
            if (evenRows) {
                coordsRow = listOf(coordsRow[1])
            }
            if (evenColumns) {
                coordsCol = listOf(coordsCol[1])
            }
        }
        arrangeDirtyFlag = false
        onArrange()
    }
    /**
     * Calculate visible tiles.
     */
    public fun calculate() {
        // Do not allocate any objects in this function!
        arrange()
        lateinit var rowrange: IntProgression
        lateinit var colrange: IntProgression
        val fViewPort = rectFPool.acquire()!!
        if (viewPort.overlap(docRect)) {
            val iViewPort = rectIPool.acquire()!!
            iViewPort.intersectInto(viewPort, docRect)
            fViewPort.iSet(iViewPort)
            rectIPool.release(iViewPort)
        } else {
            throw IndexOutOfBoundsException()
        }
        if (evenRows) {
            // simplified algorithm if pages have equal heights
            val from = floor(fViewPort.top / coordsRow[0]).toInt()
            val to = ceil(fViewPort.bottom / coordsRow[0]).toInt()
            rowrange = from until to
        } else {
            var i = 1
            while (fViewPort.top >= coordsRow[i]) {
                i++
            }
            i--
            var from = i
            while (fViewPort.bottom > coordsRow[i]) {
                i++
            }
            rowrange = from until i
        }
        if (evenColumns) {
            // simplified algorithm if pages have equal widths
            val from = floor(fViewPort.left / coordsCol[0]).toInt()
            val to = ceil(fViewPort.right / coordsCol[0]).toInt()
            colrange = from until to
        } else {
            var i = 1
            while (fViewPort.left >= coordsCol[i]) {
                i++
            }
            i--
            var from = i
            while (fViewPort.right > coordsCol[i]) {
                i++
            }
            colrange = from until i
        }
        pageTiles.clear()
        pageTiles.allocate(tiles.x * tiles.y * colrange.count() * rowrange.count())
        var i = 0 // page counter
        var j = 0 // tile counter
        val rollingVisibleRect = rectIPool.acquire()!!
        var rollingVisibleRectInitialized = false
        for (r in rowrange) {
            for (c in colrange) {
                val pos = rectIPool.acquire()!!
                if (evenRows) {
                    pos.top = r * coordsRow[0]
                    pos.bottom = (r + 1) * coordsRow[0]
                } else {
                    pos.top = coordsRow[r]
                    pos.bottom = coordsRow[r + 1]
                }
                if (evenColumns) {
                    pos.left = c * coordsCol[0]
                    pos.right = (c + 1) * coordsCol[0]
                } else {
                    pos.left = coordsCol[c]
                    pos.right = coordsCol[c + 1]
                }
                if (pageGrid[r, c] != null) {
                    val pageNum = pageGrid[r, c]!!
                    val pageSize = pointIPool.acquire()!!.iSet(pageGeometries[pageNum].size)
                    val pageCrop = rectFPool.acquire()!!.iSet(0f, 0f, 1f, 1f)
                    if (crop) {
                        pageCrop.iSet(pageGeometries[pageNum].crop)
                        pageSize.apply {
                            x = round(pageSize.x * pageCrop.width()).toInt()
                            y = round(pageSize.y * pageCrop.height()).toInt()
                        }
                    }
                    var pageOffset =
                            pointIPool.acquire()!!.apply {
                                x = 0
                                y = 0
                            }
                    if (!evenPages || crop) {
                        pageOffset.x = (pos.width() - padding.left - padding.right - pageSize.x) / 2
                        pageOffset.y = (pos.height() - padding.top - padding.bottom - pageSize.y) / 2
                    }
                    val pageRect = rectIPool.acquire()!!
                    pageRect.iSet(
                            pos.left + padding.left + pageOffset.x,
                            pos.top + padding.top + pageOffset.y,
                            pos.left + padding.left + pageOffset.x + pageSize.x,
                            pos.top + padding.top + pageOffset.y + pageSize.y)
                    for (rr in 0 until tiles.y) {
                        for (cc in 0 until tiles.x) {
                            if (tiles.x == 1 && tiles.y == 1) {
                                // simplified algorithm if pages arn't split into tiles
                                if (!rollingVisibleRectInitialized) {
                                    rollingVisibleRectInitialized = true
                                    rollingVisibleRect.iSet(pos)
                                } else {
                                    rollingVisibleRect.iMerge(pos)
                                }
                                pageTiles.accessNew {
                                    it.page = pageNum
                                    it.position.iSet(pageRect)
                                    it.crop.iSet(pageCrop)
                                    it.tile.iSet(0, 0)
                                    it.tiles.iSet(tiles)
                                }
                            } else {
                                val posRect = rectIPool.acquire()!!
                                val tileRect = rectIPool.acquire()!!
                                val pagePart = rectFPool.acquire()!!
                                posRect.iSet(
                                        pos.left + pos.width() * cc / tiles.x,
                                        pos.top + pos.height() * rr / tiles.y,
                                        pos.left + pos.width() * (cc + 1) / tiles.x,
                                        pos.top + pos.height() * (rr + 1) / tiles.y)
                                tileRect.intersectInto(posRect, pageRect)
                                pagePart.iSet(
                                        (tileRect.left - pageRect.left).toFloat() /
                                                pageRect.width() * pageCrop.width() + pageCrop.left,
                                        (tileRect.top - pageRect.top).toFloat() /
                                                pageRect.height() * pageCrop.height() +
                                                pageCrop.top,
                                        (tileRect.right - pageRect.left).toFloat() /
                                                pageRect.width() * pageCrop.width() + pageCrop.left,
                                        (tileRect.bottom - pageRect.top).toFloat() /
                                                pageRect.height() * pageCrop.height() +
                                                pageCrop.top)
                                if (tileRect.overlap(fViewPort)) {
                                    if (!rollingVisibleRectInitialized) {
                                        rollingVisibleRectInitialized = true
                                        rollingVisibleRect.iSet(posRect)
                                    } else {
                                        rollingVisibleRect.iMerge(posRect)
                                    }
                                    pageTiles.accessNew {
                                        it.page = pageNum
                                        it.position.iSet(tileRect)
                                        it.crop.iSet(pagePart)
                                        it.tile.iSet(cc, rr)
                                        it.tiles.iSet(tiles)
                                    }
                                    j++
                                }
                                rectIPool.release(posRect)
                                rectIPool.release(tileRect)
                                rectFPool.release(pagePart)
                            }
                        }
                    }
                    i++
                    pointIPool.release(pageOffset)
                    pointIPool.release(pageSize)
                    rectFPool.release(pageCrop)
                    rectIPool.release(pageRect)
                }
                rectIPool.release(pos)
            }
        }
        if (rollingVisibleRectInitialized) {
            visibleRect.iSet(rollingVisibleRect)
        } else {
            visibleRect.iSet(0, 0, 0, 0)
        }
        rectIPool.release(rollingVisibleRect)
        rectFPool.release(fViewPort)
    }
    /**
     * Initialize object with page sizes.
     * @param pages Page geometries
     */
    public fun initialize(pages: List<PageLayoutManager.PageGeometry>) {
        pageGeometries = pages
        pageNum = pages.size
        var firstPageSize = pageGeometries[0].size
        evenPages = pageGeometries.map { it.size == firstPageSize }.all { it }
        invalidate()
    }
    /**
     * Trigger page rearranging
     */
    public fun invalidate() {
        arrangeDirtyFlag = true
    }
    /**
     * On rearranging callback
     */
    public var onArrange: () -> Unit = {}

    private var cropNormalized = false
    /**
     * Check and fix crop boundaries
     * This is a cold function, running only once.
     */
    private fun normalizeCrop() {
        if (!cropNormalized) {
            val emptyPages = mutableListOf<Int>()
            val minCrop = RectF(0f, 0f, 1f, 1f)
            // find empty pages and set their crop to minimal of non-empties
            pageGeometries.forEachIndexed {
                index, page ->
                    if (page.crop.height() <= 0 || page.crop.width() <= 0) {
                        emptyPages.add(index)
                    } else {
                        minCrop.left = max(minCrop.left, page.crop.left)
                        minCrop.right = min(minCrop.right, page.crop.right)
                        minCrop.top = max(minCrop.top, page.crop.top)
                        minCrop.bottom = min(minCrop.bottom, page.crop.bottom)
                    }
            }
            emptyPages.forEach {
                pageGeometries[it].crop.iSet(minCrop)
            }
            cropNormalized = true
        }
    }
    /**
     * Get string representation
     * @return String representation
     */
    override fun toString(): String {
        return "PageLayoutManager[${pageNum}p;${columns}c;${rows}r]"
    }
    // {{{ scale functions
    /**
     * Get scale to fit document into screen
     * @param viewSize Size of the screen
     * @return Scale value
     */
    public fun getScale(viewSize: PointI): Float =
        if (docRect.height() > docRect.width()) {
            getScale(viewSize, Direction.VERTICAL)
        } else {
            getScale(viewSize, Direction.HORIZONTAL)
        }
    /**
     * Get scale to fit document into screen
     * @param viewSize Size of the screen
     * @param layout Document layout
     * @return Scale value
     */
    public fun getScale(viewSize: PointI, layout: Direction): Float =
        when (layout) {
            Direction.VERTICAL -> viewSize.x.toFloat() / docRect.width().toFloat()
            Direction.HORIZONTAL -> viewSize.y.toFloat() / docRect.height().toFloat()
        }
    // }}}
    /**
     * Object holds page geometry properties
     * @property size Original page size
     * @property crop Normalized crop rectangle
     */
    public class PageGeometry(val size: PointI, var crop: RectF) : Parcelable {
        constructor(parcel: Parcel) : this(PointI(), RectF()) {
            size.x = parcel.readInt()
            size.y = parcel.readInt()
            crop.left = parcel.readFloat()
            crop.top = parcel.readFloat()
            crop.right = parcel.readFloat()
            crop.bottom = parcel.readFloat()
        }

        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeInt(size.x)
            parcel.writeInt(size.y)
            parcel.writeFloat(crop.left)
            parcel.writeFloat(crop.top)
            parcel.writeFloat(crop.right)
            parcel.writeFloat(crop.bottom)
        }

        override fun toString(): String {
            return "PageGeometry[${size};${crop}]"
        }

        companion object {
            val CREATOR: Parcelable.Creator<PageGeometry?>
                = object : Parcelable.Creator<PageGeometry?> {
                    override fun createFromParcel(parcel: Parcel): PageGeometry? {
                        return PageGeometry(parcel)
                    }
                    override fun newArray(size: Int): Array<PageGeometry?> {
                        return arrayOfNulls(size)
                    }
                }
        }
    }
    /**
     * Object holds page tile properties
     * @property page Page number
     * @property position Tile position
     * @property crop Tile normalized crop rectangle
     * @property tile Current tile index
     * @property tiles Number of page tiles
     */
    public data class PageTile(
            var page: Int,
            var position: RectI,
            var crop: RectF,
            var tile: PointI,
            var tiles: PointI,
    )
    /**
     * Write state to parcel.
     * @param parcel State parcel
     */
    public fun writeToParcel(parcel: PageLayoutManagerParcel) : PageLayoutManagerParcel {
        parcel.pageFlow = pageFlow.ordinal
        parcel.gridDirection = gridDirection.ordinal
        parcel.leftToRight = leftToRight
        parcel.topToBottom = topToBottom
        parcel.columns = columns
        parcel.rows = rows
        parcel.padding.rect = padding
        parcel.viewPort.rect = viewPort
        parcel.pageGeometries = pageGeometries
        return parcel
    }
    /**
     * Write state to parcel.
     */
    public fun writeToParcel() : PageLayoutManagerParcel = writeToParcel(PageLayoutManagerParcel())
    /**
     * Read state from parcel.
     * @param parcel State parcel
     */
    public fun readFromParcel(parcel: PageLayoutManagerParcel) {
        pageFlow = Direction.values()[parcel.pageFlow]
        gridDirection = Direction.values()[parcel.gridDirection]
        leftToRight = parcel.leftToRight
        topToBottom = parcel.topToBottom
        columns = parcel.columns
        rows = parcel.rows
        padding = parcel.padding.rect
        viewPort.iSet(parcel.viewPort.rect)
        initialize(parcel.pageGeometries)
    }
}
