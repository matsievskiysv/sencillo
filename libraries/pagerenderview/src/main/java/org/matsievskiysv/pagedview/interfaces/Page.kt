package org.matsievskiysv.pagerenderview.interfaces

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

import android.graphics.Bitmap

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * Abstract class for rendering page
 * @param pageNum Page number
 * @property pageNum Page number
 * @property pageSize Size of page
 * @property mutex Page mutex
 */
abstract class Page(public val pageNum: Int) {
    protected val mutex = Mutex()

    abstract public val pageSize: PointI
    /**
     * Execute non-blocking action with exclusive access to page
     * @param func Function to perform with page. Function argument is [Page]
     * @return Result of the function
     */
    suspend fun <R> withLock(func: (Page) -> R): R {
        return mutex.withLock {
            this.let(func)
        }
    }
    /**
     * Execute blocking action with exclusive access to page
     * @param func Function to perform with page. Function argument is [Page]
     * @return Result of the function
     */
    fun <R> withBlock(func: (Page) -> R): R {
        return runBlocking {
            withLock(func)
        }
    }
    /**
     * Close page.
     */
    open fun close() {}
    /**
     * Put page cross references into overlay.
     * @param overlay Overlay to put references into.
     */
    open fun putCrossRefs(overlay: Overlay) {}
    /**
     * Put page cross references into overlay.
     * @param overlay Overlay to put links into.
     */
    open fun putExternalLinks(overlay: Overlay) {}
    /**
     * Draw page.
     * @param bitmap Rendering bitmap
     * @param cropLeft Tile left boundary
     * @param cropTop Tile top boundary
     * @param cropRight Tile right boundary
     * @param cropBottom Tile bottom boundary
     */
    abstract fun draw(bitmap: Bitmap, cropLeft: Float, cropTop: Float, cropRight: Float, cropBottom: Float)
}
