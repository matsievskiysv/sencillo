package org.matsievskiysv.pagerenderview.helpers

import kotlin.math.ceil

/**
 * Optionally growing object pool
 * @param capacity Initial number of objects in pool
 * @param generator Object constructor
 * @param growing Allow pool growing
 * @property generator Object constructor
 * @property growing Allow pool growing
 */
class Pool<T>(capacity: Int, public var growing: Boolean, private val generator: () -> T) {

    constructor(capacity: Int, generator: () -> T) : this(capacity, false, generator)

    private val pool = MutableList(capacity) { MutablePair(false, generator()) }
    /**
     * Acquire an object from pool
     * @return Object
     */
    public fun acquire(): T? {
        for (pair in pool) {
            if (!pair.first) {
                pair.first = true
                return pair.second
            }
        }
        if (growing) {
            val size = pool.size
            val newElementsSize = ceil(size * 1.2).toInt() // 20% growth
            pool.addAll(List(newElementsSize) { MutablePair(false, generator()) })
            pool[size].first = true
            return pool[size].second
        }
        return null
    }
    /**
     * Perform action over acquired pool objects
     * @param action Action to perform over acquired pool items. Function argument is a pool item
     */
    public fun eachAquired(action: (T) -> Unit) {
        for (pair in pool) {
            if (pair.first) {
                action(pair.second)
            }
        }
    }
    /**
     * Return object to pool
     * @param object To return to pool
     */
    public fun release(obj: T) {
        for (pair in pool) {
            if (pair.second === obj) {
                pair.first = false
                return
            }
        }
    }
    /**
     * Return all acquired objects to pool
     */
    public fun releaseAll() {
        for (pair in pool) {
            pair.first = false
        }
    }
    /**
     * Container for a pair of objects
     * @property first First object
     * @property second Second object
     */
    data class MutablePair<K, V>(var first: K, var second: V)
}
