package org.matsievskiysv.pagerenderview.interfaces

import android.graphics.Bitmap

import kotlinx.coroutines.Job

import org.matsievskiysv.pagerenderview.OverlayAction
import org.matsievskiysv.pagerenderview.geometry.*

/**
 * Abstract class for object shared between overlay items.
 * @param tag Layer name asset
 * @property items Overlay items
 * @property tag Layer name asset
 * @property visible Layer visible
 * @property runCallbacks Run callbacks on layer
 * @property job Preprocessing job. This object is set externally by [org.matsievskiysv.pagerenderview.OverlayManager].
 * @property callback Overlay items
 * @property actionMap Preprocessed list for quick lookup
 */
abstract class Overlay(public var tag: Int) {
    public val items = mutableMapOf<Int, List<OverlayItem>>()
    public val empty: Boolean
        get() = items.size == 0
    public var visible = true
    public var runCallbacks = true
    private var actionMap = listOf<Triple<Int, RectI, OverlayItem>>()
    public var callback: (OverlayItem, OverlayAction) -> Unit = { _, _ -> }
    /**
     * Draw page overlay images on bitmap.
     * @param bitmap Bitmap to put image into
     * @param pageNum Page number
     * @param tileLeft Cropping boundary
     * @param tileTop Cropping boundary
     * @param tileRight Cropping boundary
     * @param tileBottom Cropping boundary
     */
    fun drawPage(bitmap: Bitmap, pageNum: Int, tileLeft: Float,
                 tileTop: Float, tileRight: Float, tileBottom: Float) {
        if (visible) {
            items[pageNum]?.forEach {
                it.withBlock {
                    it.drawOnTile(bitmap, tileLeft, tileTop, tileRight, tileBottom)
                }
            }
        }
    }
    /**
     * Preprocess overlay. This function populates [actionMap].
     */
    fun createActionMap() {
        actionMap = items.values.flatten().distinct().flatMap {
            item -> item.actionArea.map {
                Triple(item.page, it, item)
            }
        }
    }
    /**
     * Search for [OverlayItem] at position and call callback function.
     * @param page Search for [OverlayItem] at this page
     * @param point Search for [OverlayItem] at this position
     * @param type Action type
     * @return Action was called
     */
    fun callAtPoint(page: Int, point: PointI, type: OverlayAction): Boolean =
        actionMap.firstOrNull { page == it.first && point isInside it.second } ?.let {
            callback(it.third, type)
            true
        } ?: false
}
