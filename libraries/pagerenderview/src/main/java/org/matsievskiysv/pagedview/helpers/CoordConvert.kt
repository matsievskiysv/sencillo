package org.matsievskiysv.pagerenderview.helpers

import org.matsievskiysv.pagerenderview.geometry.*
import kotlin.math.*

/**
 * Convert coordinates between Original and Scaled coordinate systems
 * @constructor Create new converter
 * @property scale Scale between coordinate systems
 * @property originalRect Rectangle in original system
 * @property scaledRect Rectangle in scaled system
 * @param scale Initial scale
 * @param originalRect Rectangle object to use as original system rectangle
 * @param scaledRect Rectangle object to use as scaled system rectangle
 */
class CoordConvert(public var scale: Float = 1f,
                   public var originalRect: RectI = RectI(),
                   public var scaledRect: RectI = RectI()) {
    /**
     * Set original rectangle to value and scale scaled rectangle accordingly
     * @param left Left point
     * @param top Top point
     * @param right Right point
     * @param bottom Bottom point
     * @param scale Scale value
     */
    fun setOriginal(left: Int, top: Int, right: Int, bottom: Int, scale: Float?) {
        if (scale != null) {
            this.scale = scale
            if (scale <= 0) {
                throw IllegalArgumentException("Scale must be positive")
            }
        }
        originalRect.iSet(left, top, right, bottom)
        scaledRect.iSet(
            round(left * this.scale).toInt(),
            round(top * this.scale).toInt(),
            round(right * this.scale).toInt(),
            round(bottom * this.scale).toInt()
        )
    }
    /**
     * Set original rectangle to value and scale scaled rectangle accordingly
     * @param left Left point
     * @param top Top point
     * @param right Right point
     * @param bottom Bottom point
     */
    fun setOriginal(left: Int, top: Int, right: Int, bottom: Int) = setOriginal(left, top, right, bottom, null)
    /**
     * Set original rectangle to value and scale scaled rectangle accordingly
     * @param rect New rectangle
     * @param scale Scale value
     */
    fun setOriginal(rect: RectI, scale: Float?) = setOriginal(rect.left, rect.top, rect.right, rect.bottom, scale)
    /**
     * Set original rectangle to value and scale scaled rectangle accordingly
     * @param rect New rectangle
     */
    fun setOriginal(rect: RectI) = setOriginal(rect.left, rect.top, rect.right, rect.bottom, null)
    /**
     * Set scaled rectangle to value and scale original rectangle accordingly
     * @param left Left point
     * @param top Top point
     * @param right Right point
     * @param bottom Bottom point
     * @param scale Scale value
     */
    fun setScaled(left: Int, top: Int, right: Int, bottom: Int, scale: Float?) {
        if (scale != null) {
            this.scale = scale
            if (scale <= 0) {
                throw IllegalArgumentException("Scale must be positive")
            }
        }
        scaledRect.iSet(left, top, right, bottom)
        originalRect.iSet(
            round(left / this.scale).toInt(),
            round(top / this.scale).toInt(),
            round(right / this.scale).toInt(),
            round(bottom / this.scale).toInt()
        )
    }
    /**
     * Set scaled rectangle to value and scale original rectangle accordingly
     * @param left Left point
     * @param top Top point
     * @param right Right point
     * @param bottom Bottom point
     */
    fun setScaled(left: Int, top: Int, right: Int, bottom: Int) = setScaled(left, top, right, bottom, null)
    /**
     * Set scaled rectangle to value and scale original rectangle accordingly
     * @param rect New rectangle
     * @param scale Scale value
     */
    fun setScaled(rect: RectI, scale: Float?) = setScaled(rect.left, rect.top, rect.right, rect.bottom, scale)
    /**
     * Set scaled rectangle to value and scale original rectangle accordingly
     * @param rect New rectangle
     */
    fun setScaled(rect: RectI) = setScaled(rect.left, rect.top, rect.right, rect.bottom, null)
    /**
     * Move systems
     * @param deltaX X shift
     * @param deltaY Y shift
     */
    fun moveOriginal(deltaX: Int, deltaY: Int) = setOriginal(originalRect.left + deltaX,
                                                             originalRect.top + deltaY,
                                                             originalRect.right + deltaX,
                                                             originalRect.bottom + deltaY)
    /**
     * Move systems
     * @param delta Shift
     */
    fun moveOriginal(delta: PointI) = moveOriginal(delta.x, delta.y)
    /**
     * Move systems
     * @param deltaX X shift
     * @param deltaY Y shift
     */
    fun moveScaled(deltaX: Int, deltaY: Int) = setScaled(scaledRect.left + deltaX,
                                                         scaledRect.top + deltaY,
                                                         scaledRect.right + deltaX,
                                                         scaledRect.bottom + deltaY)
    /**
     * Move systems
     * @param delta Shift
     */
    fun moveScaled(delta: PointI) = moveScaled(delta.x, delta.y)
    /**
     * Move systems to position
     * @param x X coordinate
     * @param y Y coordinate
     */
    fun moveOriginalTo(x: Int, y: Int) = setOriginal(x, y, x + originalRect.width(), y + originalRect.height())
    /**
     * Move systems to position
     * @param to Position
     */
    fun moveOriginalTo(to: PointI) = moveOriginalTo(to.x, to.y)
    /**
     * Move systems to position
     * @param x X coordinate
     * @param y Y coordinate
     */
    fun moveScaledTo(x: Int, y: Int) = setScaled(x, y, x + scaledRect.width(), y + scaledRect.height())
    /**
     * Move systems to position
     * @param to Position
     */
    fun moveScaledTo(to: PointI) = moveScaledTo(to.x, to.y)
    /**
     * Apply scaling of the original rectangle
     * @param newScale Scale value
     */
    fun rescaleFromOriginal(newScale: Float) = setOriginal(originalRect, newScale)
    /**
     * Apply scaling of the scaled rectangle
     * @param newScale Scale value
     */
    fun rescaleFromScaled(newScale: Float) = setOriginal(scaledRect, newScale)
    /**
     * Update original coordinates
     * This is needed only scaledRect was changed externally
     */
    fun updateOriginal() = moveScaled(0, 0)
    /**
     * Update scaled coordinates
     * This is needed only originalRect was changed externally
     */
    fun updateScaled() = moveOriginal(0, 0)
    /**
     * Crop original rectangle
     * @param width Rectangle width
     * @param height Rectangle height
     */
    fun cropOriginal(width: Int, height: Int) = setOriginal(originalRect.left,
                                                            originalRect.top,
                                                            originalRect.left + width,
                                                            originalRect.top + height)
    /**
     * Crop original rectangle
     * @param size Crop size
     */
    fun cropOriginal(size: PointI) = cropOriginal(size.x, size.y)
    /**
     * Crop scaled rectangle
     * @param width Rectangle width
     * @param height Rectangle height
     */
    fun cropScaled(width: Int, height: Int) = setScaled(scaledRect.left,
                                                        scaledRect.top,
                                                        scaledRect.left + width,
                                                        scaledRect.top + height)
    /**
     * Crop scaled rectangle
     * @param size Crop size
     */
    fun cropScaled(size: PointI) = cropScaled(size.x, size.y)
    /**
     * Get string representation
     * @return String representation
     */
    override fun toString(): String {
        return "${originalRect}:${scale}:${scaledRect}"
    }
}
