package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * [RectF] state serialization.
 * @property rect RectF
 * @param rect RectF
 */
public class RectFParcel(public var rect: RectF) : Parcelable {

    constructor() : this(RectF())

    constructor(parcel: Parcel) : this(RectF()) {
        val left = parcel.readFloat()
        val top = parcel.readFloat()
        val right = parcel.readFloat()
        val bottom = parcel.readFloat()
        rect.iSet(left, top, right, bottom)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(rect.left)
        parcel.writeFloat(rect.top)
        parcel.writeFloat(rect.right)
        parcel.writeFloat(rect.bottom)
    }

    companion object {
        val CREATOR: Parcelable.Creator<RectFParcel?>
            = object : Parcelable.Creator<RectFParcel?> {
                override fun createFromParcel(parcel: Parcel): RectFParcel? {
                    return RectFParcel(parcel)
                }
                override fun newArray(size: Int): Array<RectFParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
