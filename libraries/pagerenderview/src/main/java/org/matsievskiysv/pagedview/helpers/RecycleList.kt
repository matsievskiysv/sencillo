package org.matsievskiysv.pagerenderview.helpers

import kotlin.math.ceil

/**
 * Special list for recycling frequently recreated lists.
 * @param initialSize Initial size of the list
 * @param generator Constructor for new objects
 * @property generator Constructor for new objects
 * @property size Size of list
 */
class RecycleList<T>(initialSize: Int, private val generator: () -> T) {

    public var size = 0
    private val objs = MutableList(initialSize) { generator() }
    /**
     * Allocate new list item and do action over it
     * @param action Action to perform over item. Function argument is list item
     * @return Return new item index
     */
    public fun accessNew(action: (T) -> Unit): Int {
        size += 1
        if (size > objs.size) {
            val newElementsSize = ceil(objs.size * 0.2).toInt() // 20% growth
            objs.addAll(List(newElementsSize) { generator() })
        }
        objs[size - 1].let(action)
        return size - 1
    }
    /**
     * Do action over item
     * @param ind List index
     * @param action Action to perform over item. Function argument is list item
     */
    public fun access(ind: Int, action: (T) -> Unit) {
        objs[ind].let(action)
    }
    /**
     * Allocate new list item
     * @param cap Allocate items to match capacity
     */
    public fun allocate(cap: Int) {
        if (objs.size < cap) {
            objs.addAll(List(cap - objs.size) { generator() })
        }
    }
    /**
     * Do action over list items
     * @param action Action to perform over list items. Function argument is list item
     */
    public fun forEach(action: (T) -> Unit) {
        for (i in 0 until size) {
            objs[i].let(action)
        }
    }
    /**
     * Clear list items. Memory is not freed, use [shrink] to free memory afterwords.
     */
    public fun clear() {
        size = 0
    }
    /**
     * Free allocated but unused memory
     */
    public fun shrink() {
        val newSize = ceil(objs.size * 1.2).toInt() // 20% of current size
        while (objs.size > newSize) {
            objs.removeLastOrNull()
        }
    }
}
