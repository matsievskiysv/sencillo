package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * [PointF] state serialization.
 * @property point PointF
 * @param point PointF
 */
public class PointFParcel(public var point: PointF) : Parcelable {

    constructor() : this(PointF())

    constructor(parcel: Parcel) : this(PointF()) {
        val x = parcel.readFloat()
        val y = parcel.readFloat()
        point.iSet(x, y)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(point.x)
        parcel.writeFloat(point.y)
    }

    companion object {
        val CREATOR: Parcelable.Creator<PointFParcel?>
            = object : Parcelable.Creator<PointFParcel?> {
                override fun createFromParcel(parcel: Parcel): PointFParcel? {
                    return PointFParcel(parcel)
                }
                override fun newArray(size: Int): Array<PointFParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
