package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * [PointI] state serialization.
 * @property point PointF
 * @param point PointF
 */
public class PointIParcel(public var point: PointI) : Parcelable {

    constructor() : this(PointI())

    constructor(parcel: Parcel) : this(PointI()) {
        val x = parcel.readInt()
        val y = parcel.readInt()
        point.iSet(x, y)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(point.x)
        parcel.writeInt(point.y)
    }

    companion object {
        val CREATOR: Parcelable.Creator<PointIParcel?>
            = object : Parcelable.Creator<PointIParcel?> {
                override fun createFromParcel(parcel: Parcel): PointIParcel? {
                    return PointIParcel(parcel)
                }
                override fun newArray(size: Int): Array<PointIParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
