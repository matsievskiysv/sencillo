package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import kotlin.math.*

import org.matsievskiysv.pagerenderview.PageLayoutManager
import org.matsievskiysv.pagerenderview.geometry.*

/**
 * [PageLayoutManager] state serialization.
 */
public class PageLayoutManagerParcel() : Parcelable {
    /**
     * Page flow direction
     */
    public var pageFlow = 0
    /**
     * Grid resize direction
     */
    public var gridDirection = 0
    /**
     * Left to right flag
     */
    public var leftToRight = false
    /**
     * Top to bottom flag
     */
    public var topToBottom = false
    /**
     * Number of columns
     */
    public var columns = 0
    /**
     * Number of rows
     */
    public var rows = 0
    /**
     * Page padding.
     */
    public var padding = RectIParcel()
    /**
     * Cropping state
     */
    public var crop = false
    /**
     * View port ancor point
     */
    public var viewPort = RectIParcel()
    /**
     * Crop bounds found flag. This value is used for [PageLayoutManager] initialization.
     */
    public var pageGeometries = listOf<PageLayoutManager.PageGeometry>()
    /**
     * View size. This value is used during [ViewRectMapper] initialization before actual size is available.
     */
    public var viewSize = PointIParcel()

    constructor(parcel: Parcel) : this() {
        pageFlow = parcel.readInt()
        gridDirection = parcel.readInt()
        leftToRight = parcel.readBoolean()
        topToBottom = parcel.readBoolean()
        columns = parcel.readInt()
        rows = parcel.readInt()
        padding = parcel.readParcelable(null)!!
        viewPort = parcel.readParcelable(null)!!
        pageGeometries = parcel.readParcelableList(mutableListOf<PageLayoutManager.PageGeometry>(), null)
        viewSize = parcel.readParcelable(null)!!
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(pageFlow)
        parcel.writeInt(gridDirection)
        parcel.writeBoolean(leftToRight)
        parcel.writeBoolean(topToBottom)
        parcel.writeInt(columns)
        parcel.writeInt(rows)
        parcel.writeParcelable(padding, 0)
        parcel.writeParcelable(viewPort, 0)
        parcel.writeParcelableList(pageGeometries, 0)
        parcel.writeParcelable(viewSize, 0)
    }

    companion object {
        val CREATOR: Parcelable.Creator<PageLayoutManagerParcel?>
            = object : Parcelable.Creator<PageLayoutManagerParcel?> {
                override fun createFromParcel(parcel: Parcel): PageLayoutManagerParcel? {
                    return PageLayoutManagerParcel(parcel)
                }
                override fun newArray(size: Int): Array<PageLayoutManagerParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
