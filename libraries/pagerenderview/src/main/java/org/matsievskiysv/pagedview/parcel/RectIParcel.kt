package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * [RectI] state serialization.
 * @property rect RectI
 * @param rect RectI
 */
public class RectIParcel(public var rect: RectI) : Parcelable {

    constructor() : this(RectI())

    constructor(parcel: Parcel) : this(RectI()) {
        val left = parcel.readInt()
        val top = parcel.readInt()
        val right = parcel.readInt()
        val bottom = parcel.readInt()
        rect.iSet(left, top, right, bottom)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(rect.left)
        parcel.writeInt(rect.top)
        parcel.writeInt(rect.right)
        parcel.writeInt(rect.bottom)
    }

    companion object {
        val CREATOR: Parcelable.Creator<RectIParcel?>
            = object : Parcelable.Creator<RectIParcel?> {
                override fun createFromParcel(parcel: Parcel): RectIParcel? {
                    return RectIParcel(parcel)
                }
                override fun newArray(size: Int): Array<RectIParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
