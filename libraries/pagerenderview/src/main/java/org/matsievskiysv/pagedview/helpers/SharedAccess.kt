package org.matsievskiysv.pagerenderview.helpers

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

/**
 * Make object thread safe
 * @param asset Object
 * @property asset Object
 * @property mutex Object mutex
 */
public class SharedAccess<T> (private val asset: T) {
    private val mutex = Mutex()
    /**
     * Make suspend call with object as argument
     * @param func Function to call with object as argument
     * @return Function evaluation result
     */
    suspend fun <R> withLock(func: (T) -> R): R {
        return mutex.withLock {
            asset.let(func)
        }
    }
    /**
     * Make blocking call with object as argument
     * @param func Function to call with object as argument
     * @return Function evaluation result
     */
    fun <R> withBlock(func: (T) -> R): R {
        return runBlocking {
            withLock(func)
        }
    }
}
