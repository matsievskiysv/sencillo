package org.matsievskiysv.pagerenderview.interfaces

import android.graphics.Bitmap

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * Abstract class for overlay item.
 * @param page Page to which item belongs
 * @property page Page to which item belongs
 * @param group Item group
 * @property group Item group
 * @property mutex Item mutex
 * @property actionArea List of rectangles, responsive to actions.
 */
abstract class OverlayItem(public val page: Int, open public val overlay: Overlay) {
    open protected val mutex = Mutex()
    abstract public val actionArea: List<RectI>
    /**
     * Draw overlay item on page tile.
     * Single [OverlayItem] may use multiple [OverlayDrawable] objects to draw a complex geometry.
     * @param bitmap Drawing bitmap
     * @param cropLeft Tile left boundary
     * @param cropTop Tile top boundary
     * @param cropRight Tile right boundary
     * @param cropBottom Tile bottom boundary
     */
    abstract fun drawOnTile(bitmap: Bitmap, cropLeft: Float, cropTop: Float, cropRight: Float, cropBottom: Float)
    /**
     * Execute non-blocking action with exclusive access to item
     * @param func Function to perform with item. Function argument is [OverlayItem]
     * @return Result of the function
     */
    open suspend fun <R> withLock(func: (OverlayItem) -> R): R {
        return mutex.withLock {
            this.let(func)
        }
    }
    /**
     * Execute blocking action with exclusive access to item
     * @param func Function to perform with item. Function argument is [OverlayItem]
     * @return Result of the function
     */
    fun <R> withBlock(func: (OverlayItem) -> R): R {
        return runBlocking {
            withLock(func)
        }
    }
}
