package org.matsievskiysv.pagerenderview

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.*

import io.kotest.property.checkAll
import io.kotest.property.Arb
import io.kotest.property.arbitrary.*

import org.matsievskiysv.pagerenderview.helpers.LRUCache

// TODO: replace with property tests where possible
class LRUCacheTest : StringSpec({
    "same size inserts" {
        val cache = LRUCache<Int, String>(5)
	    cache.clear()
        // push
        cache.push(10, "a")
	    cache.push(20, "b")
	    cache.push(30, "c")
        cache.getObjects() shouldBe listOf("c", "b", "a")
	    cache.push(40, "d")
	    cache.push(50, "e")
        cache.getLabels() shouldBe listOf(50, 40, 30, 20, 10)
        cache.getObjects() shouldBe listOf("e", "d", "c", "b", "a")
        cache.push(60, "f")
        cache.getObjects() shouldBe listOf("f", "e", "d", "c", "b")
        cache.push(30, "g")
        cache.getObjects() shouldBe listOf("g", "f", "e", "d", "b")
        cache.push(40, "d")
        cache.getObjects() shouldBe listOf("d", "g", "f", "e", "b")
        // remove
        cache.pop(60) shouldBe "f"
        cache.getObjects() shouldBe listOf("d", "g", "e", "b")
        cache.pop(30) shouldBe "g"
        cache.getObjects() shouldBe listOf("d", "e", "b")
        cache.push(10, "a")
        cache.getObjects() shouldBe listOf("a", "d", "e", "b")
        cache.push(20, "b")
        cache.push(30, "c")
        cache.getObjects() shouldBe listOf("c", "b", "a", "d", "e")
        cache.push(60, "f")
        cache.getObjects() shouldBe listOf("f", "c", "b", "a", "d")
        cache.get(10) shouldBe "a"
        cache.getObjects() shouldBe listOf("a", "f", "c", "b", "d")
        cache.get(30) shouldBe "c"
        cache.getObjects() shouldBe listOf("c", "a", "f", "b", "d")
        // capacity
        cache.setCapacity(3)
        cache.getObjects() shouldBe listOf("c", "a", "f")
        cache.get(10) shouldBe "a"
        cache.getObjects() shouldBe listOf("a", "c", "f")
        cache.push(20, "b")
        cache.getObjects() shouldBe listOf("b", "a", "c")
        cache.setCapacity(6)
        cache.push(1, "x")
        cache.push(2, "y")
        cache.push(3, "z")
        cache.getObjects() shouldBe listOf("z", "y", "x", "b", "a", "c")
    }
    "initializer/destructor" {
        var initCounter = 0
        var destCounter = 0
        val cache = LRUCache<Int, String>(3,
                                       initializer = { _, _ -> initCounter++ },
                                       destructor = { _, _ -> destCounter++ })
	    cache.clear()
        // push
        initCounter shouldBe 0
        cache.push(10, "a")
        initCounter shouldBe 1
        cache.push(20, "b")
        initCounter shouldBe 2
        cache.push(30, "c", callInit=false)
        initCounter shouldBe 2
        destCounter shouldBe 0
        cache.push(10, "a")
        initCounter shouldBe 2
        destCounter shouldBe 0
        cache.push(40, "d")
        initCounter shouldBe 3
        destCounter shouldBe 1
        cache.push(10, "a", callInit=false, callDestruct=false)
        initCounter shouldBe 3
        destCounter shouldBe 1
        cache.push(20, "b", callInit=true, callDestruct=false)
        initCounter shouldBe 4
        destCounter shouldBe 1
        cache.push(50, "d", callInit=true, callDestruct=true)
        initCounter shouldBe 5
        destCounter shouldBe 2
    }
    "different size inserts" {
        val cache = LRUCache<Int, String>(500)
	    cache.clear()
        cache.push(10, "a", 200)
        cache.getObjects() shouldBe listOf("a")
	    cache.push(20, "b", 100)
        cache.getObjects() shouldBe listOf("b", "a")
	    cache.push(30, "c", 300)
        cache.getObjects() shouldBe listOf("c", "b")
	    cache.push(40, "d", 100)
        cache.getObjects() shouldBe listOf("d", "c", "b")
	    cache.push(50, "e", 100)
        cache.getObjects() shouldBe listOf("e", "d", "c")
	    cache.pop(30)
        cache.getObjects() shouldBe listOf("e", "d")
	    cache.push(10, "a", 100)
        cache.getObjects() shouldBe listOf("a", "e", "d")
	    cache.push(30, "c", 100)
        cache.getObjects() shouldBe listOf("c", "a", "e", "d")
	    cache.push(20, "b", 100)
        cache.getObjects() shouldBe listOf("b", "c", "a", "e", "d")
	    cache.push(10, "a", 300)
        cache.getObjects() shouldBe listOf("a", "b", "c")
	    cache.clear()
	    cache.push(10, "a", 100)
	    cache.push(20, "b", 100)
	    cache.push(30, "c", 100)
	    cache.push(40, "d", 100)
	    cache.push(50, "e", 100)
        cache.getObjects() shouldBe listOf("e", "d", "c", "b", "a")
    }
})
