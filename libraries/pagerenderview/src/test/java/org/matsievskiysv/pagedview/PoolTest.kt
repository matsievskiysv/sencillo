// package org.matsievskiysv.pagerenderview

// import kotlin.math.*

// import io.kotest.core.spec.style.StringSpec
// import io.kotest.matchers.*
// import io.kotest.matchers.nulls.*

// import org.matsievskiysv.pagerenderview.helpers.Pool
// import org.matsievskiysv.pagerenderview.geometry.*


// class PoolTest : StringSpec({
//     "PointI" {
//         val pool = Pool<PointI>(2) { PointI() }
//         var p1 = pool.acquire()
//         var p2 = pool.acquire()
//         p1.shouldNotBeNull()
//         p2.shouldNotBeNull()
//         var p3 = pool.acquire()
//         p3.shouldBeNull()
//         pool.release(p1)
//         p3 = pool.acquire()
//         p3.shouldNotBeNull()
//     }
// })
