#include "geometry.h"

#include <jni.h>

struct PointIRef {
	jclass	 PointI;
	jfieldID xID;
	jfieldID yID;
} pointIRef;

void
PointIRef_init(JNIEnv *env)
{
	jclass PointITmp = (*env)->FindClass(env, "org/matsievskiysv/pagerenderview/geometry/PointI");
	pointIRef.PointI = (jclass)(*env)->NewGlobalRef(env, PointITmp);
	(*env)->DeleteLocalRef(env, PointITmp);

	pointIRef.xID = (*env)->GetFieldID(env, pointIRef.PointI, "x", "I");
	pointIRef.yID = (*env)->GetFieldID(env, pointIRef.PointI, "y", "I");
}

void
PointIRef_free(JNIEnv *env)
{
	(*env)->DeleteGlobalRef(env, pointIRef.PointI);
}

void
PointI_set(JNIEnv *env, jobject point, PointI val)
{
	(*env)->SetIntField(env, point, pointIRef.xID, val.x);
	(*env)->SetIntField(env, point, pointIRef.yID, val.y);
}

PointI
PointI_get(JNIEnv *env, jobject point)
{
	PointI p;
	p.x = (*env)->GetIntField(env, point, pointIRef.xID);
	p.y = (*env)->GetIntField(env, point, pointIRef.yID);
	return p;
}

struct RectIRef {
	jclass	 RectI;
	jfieldID leftID;
	jfieldID topID;
	jfieldID rightID;
	jfieldID bottomID;
} rectIRef;

void
RectIRef_init(JNIEnv *env)
{
	jclass RectITmp = (*env)->FindClass(env, "org/matsievskiysv/pagerenderview/geometry/RectI");
	rectIRef.RectI	= (jclass)(*env)->NewGlobalRef(env, RectITmp);
	(*env)->DeleteLocalRef(env, RectITmp);

	rectIRef.leftID	  = (*env)->GetFieldID(env, rectIRef.RectI, "left", "I");
	rectIRef.topID	  = (*env)->GetFieldID(env, rectIRef.RectI, "top", "I");
	rectIRef.rightID  = (*env)->GetFieldID(env, rectIRef.RectI, "right", "I");
	rectIRef.bottomID = (*env)->GetFieldID(env, rectIRef.RectI, "bottom", "I");
}

void
RectIRef_free(JNIEnv *env)
{
	(*env)->DeleteGlobalRef(env, rectIRef.RectI);
}

void
RectI_set(JNIEnv *env, jobject rect, RectI val)
{
	(*env)->SetIntField(env, rect, rectIRef.leftID, val.left);
	(*env)->SetIntField(env, rect, rectIRef.topID, val.top);
	(*env)->SetIntField(env, rect, rectIRef.rightID, val.right);
	(*env)->SetIntField(env, rect, rectIRef.bottomID, val.bottom);
}

RectI
RectI_get(JNIEnv *env, jobject rect)
{
	RectI p;
	p.left	 = (*env)->GetIntField(env, rect, rectIRef.leftID);
	p.top	 = (*env)->GetIntField(env, rect, rectIRef.topID);
	p.right	 = (*env)->GetIntField(env, rect, rectIRef.rightID);
	p.bottom = (*env)->GetIntField(env, rect, rectIRef.bottomID);
	return p;
}
