package org.matsievskiysv.mupdfbackend

import com.artifex.mupdf.fitz.Document as FDocument
import com.artifex.mupdf.fitz.SeekableInputStream

import org.matsievskiysv.pagedview.interfaces.Document

class MuPdfDocument(stream: SeekableInputStream, mime: String): Document() {

    private val document = FDocument.openDocument(stream, mime)

    // close document
    override fun close() = document.destroy()
    // // document is password protected
    // fun needsPassword(): Boolean { return false }
    // // authenticate
    // fun authenticate(password: String): Boolean { return true }
    // // get number of pages
    override fun pageNum(): Int = document.countPages()
    // // get page
    override fun page(page: Int): MuPdfPage = MuPdfPage(document.loadPage(page))
}
