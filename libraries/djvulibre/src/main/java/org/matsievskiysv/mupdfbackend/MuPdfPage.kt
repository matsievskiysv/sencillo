package org.matsievskiysv.mupdfbackend

// import android.graphics.Point
import android.graphics.Bitmap
// import android.graphics.RectF

import com.artifex.mupdf.fitz.Page as FPage
// import com.artifex.mupdf.fitz.Rect
import com.artifex.mupdf.fitz.Matrix
import com.artifex.mupdf.fitz.android.AndroidDrawDevice

import org.matsievskiysv.pagedview.interfaces.Page
import org.matsievskiysv.pagedview.geometry.*

class MuPdfPage(private val page: FPage): Page() {

    override val pageSize: PointI

    init {
        val bounds = RectI(RectF(page.getBounds()).round())
        pageSize = PointI(bounds.width(), bounds.height())
    }

    override fun close() = page.destroy()

    override fun draw(bitmap: Bitmap, cropLeft: Float, cropTop: Float, cropRight: Float, cropBottom: Float) {
        val ctm = Matrix()
        val sizeX = bitmap.getWidth()
        val sizeY = bitmap.getHeight()
        val scaleX = sizeX.toFloat() / (cropRight - cropLeft) / pageSize.x
        val scaleY = sizeY.toFloat() / (cropBottom - cropTop) / pageSize.y
        ctm.scale(scaleX, scaleY)                                   // scale whole page to fit
        ctm.e = -cropLeft * pageSize.x * scaleX                     // move slice to the left
        ctm.f = -cropTop * pageSize.y * scaleY                      // move slice to the up
        val dev = AndroidDrawDevice(bitmap, 0, 0, 0, 0, sizeX, sizeY)
        page.run(dev, ctm, null);
        dev.close();
        dev.destroy();
    }
}
