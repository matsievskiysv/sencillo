package org.matsievskiysv.mupdfbackend

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint

import org.matsievskiysv.pagerenderview.geometry.*
import org.matsievskiysv.pagerenderview.drawable.RectangleDrawable
import org.matsievskiysv.pagerenderview.interfaces.OverlayItem
import org.matsievskiysv.pagerenderview.interfaces.Overlay

/**
 * Cross reference link
 * @param page Link from page
 * @param pageSize Size of page
 * @param linkArea Link from rectangle
 * @param pageTo Link to page
 * @param pointTo Link to point
 * @param overlay Parent overlay
 */
class CrossReference(page: Int, pageSize: PointI, linkArea: RectI, public val pageTo: Int,
                     public val pointTo: PointI, overlay: ReferenceOverlay): OverlayItem(page, overlay) {
    private val drawable = RectangleDrawable(pageSize, linkArea, overlay.canvas, overlay.paint)
    override val actionArea = listOf(linkArea)

    override fun drawOnTile(bitmap: Bitmap, cropLeft: Float, cropTop: Float, cropRight: Float, cropBottom: Float) {
        drawable.draw(bitmap, cropLeft, cropTop, cropRight, cropBottom)
    }
}
