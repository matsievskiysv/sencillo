package org.matsievskiysv.sencillo

import android.app.AlertDialog
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceDataStore
import androidx.preference.PreferenceFragmentCompat

import kotlin.reflect.KProperty1
import kotlin.reflect.KMutableProperty1

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

import org.matsievskiysv.sencillo.databinding.SettingsBinding

class DocSettingsActivity: AppCompatActivity() {
    private lateinit var binding: SettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SettingsBinding.inflate(layoutInflater)
        setSupportActionBar(binding.globalSettingsToolbar)

        val intent = getIntent()
        val uri = intent.getData()!!
        val path = RealPathUtil.getRealPath(this, uri)!!
        val db = UiDatabaseBuilder.getInstance(this)
        lateinit var item: UiItem

        runBlocking {
            launch(Dispatchers.IO) {
                item = db.uiItemDao().getByPath(path)!!
            }
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.global_settings_placeholder, BookSettingsFragment(db, item))
            .commit()

        binding.globalSettingsToolbar.setNavigationOnClickListener {
            finish()
        }

        setContentView(binding.root)
    }

    class BookSettingsFragment(private val db: UiDatabase,
                               private var item: UiItem) : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            getPreferenceManager().setPreferenceDataStore(
                object : PreferenceDataStore() {
                    private fun <T> getField(key: String): T? {
                        runBlocking{
                            launch(Dispatchers.IO) {
                                item = db.uiItemDao().getByPath(item.path)!!
                            }
                        }
                        @Suppress("UNCHECKED_CAST")
                        val property = item::class.members
                            .first { it.name == key } as KProperty1<UiItem, T>
                        // check your schema is you get exception here
                        return property.get(item)
                    }

                    override fun getBoolean(key: String, defValue: Boolean): Boolean {
                        return getField<Boolean?>(key) ?: defValue
                    }

                    override fun getFloat(key: String, defValue: Float): Float {
                        return getField<Float?>(key) ?: defValue
                    }

                    override fun getInt(key: String, defValue: Int): Int {
                        return getField<Int?>(key) ?: defValue
                    }

                    override fun getLong(key: String, defValue: Long): Long {
                        return getField<Long?>(key) ?: defValue
                    }

                    override fun getString(key: String, defValue: String?): String? {
                        return getField<String?>(key) ?: defValue
                    }

                    override fun getStringSet(key: String, defValue: MutableSet<String>?): MutableSet<String>? {
                        return getField<MutableSet<String>?>(key) ?: defValue
                    }

                    private fun <T> setField(key: String, value: T) {
                        @Suppress("UNCHECKED_CAST")
                        val property = item::class.members
                            .first { it.name == key } as KMutableProperty1<UiItem, T>
                        property.set(item, value)
                        // do not use lifecycleScope here
                        // we don't need this job to be killed
                        MainScope().launch(Dispatchers.IO) {
                            db.uiItemDao().updateItem(item)
                        }
                    }

                    override fun putBoolean(key: String, value: Boolean) = setField(key, value)

                    override fun putFloat(key: String, value: Float) = setField(key, value)

                    override fun putInt(key: String, value: Int) = setField(key, value)

                    override fun putLong(key: String, value: Long) = setField(key, value)

                    override fun putString(key: String, value: String?) = setField(key, value)

                    override fun putStringSet(key: String, value: MutableSet<String>?) = setField(key, value)
                }
            )
            setPreferencesFromResource(R.xml.book_preferences, rootKey)
        }
    }
}
