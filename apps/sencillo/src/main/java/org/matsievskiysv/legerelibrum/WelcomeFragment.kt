package org.matsievskiysv.sencillo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

import org.matsievskiysv.sencillo.databinding.FragmentWelcomeBinding

class WelcomeFragment : Fragment(R.layout.fragment_welcome) {
    private var binding: FragmentWelcomeBinding? = null
    private var text = ""
    private var buttonLabel = ""
    private var action: (Any) -> Unit = {}

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_welcome, container, false)
        binding = FragmentWelcomeBinding.bind(view)
        binding?.welcomeText?.text = text
        binding?.welcomeButton?.text = buttonLabel
        binding?.welcomeButton?.setOnClickListener(action)
        return view
    }

    fun setArgs(text: String, label: String, action: (Any) -> Unit) {
        this.text = text
        this.buttonLabel = label
        this.action = action
        binding?.welcomeText?.text = text
        binding?.welcomeButton?.text = label
        binding?.welcomeButton?.setOnClickListener(action)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }
}
