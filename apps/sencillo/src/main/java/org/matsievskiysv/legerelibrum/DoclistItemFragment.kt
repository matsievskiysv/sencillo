// package org.matsievskiysv.sencillo

// import android.os.Bundle
// import android.view.LayoutInflater
// import android.view.View
// import android.view.ViewGroup
// import androidx.fragment.app.Fragment

// import org.matsievskiysv.sencillo.databinding.FragmentDoclistItemBinding

// class DoclistItemFragment : Fragment(R.layout.fragment_doclist_item) {
//     private var binding: FragmentDoclistItemBinding? = null
//     private var title = ""
//     // private var buttonLabel = ""
//     // private var action: (Any) -> Unit = {}

//     override fun onCreateView(
//             inflater: LayoutInflater,
//             container: ViewGroup?,
//             savedInstanceState: Bundle?
//     ): View? {
//         super.onCreate(savedInstanceState)
//         val view = inflater.inflate(R.layout.fragment_welcome, container, false)
//         binding = FragmentDoclistItemBinding.bind(view)
//         binding?.title?.text = title
//         // binding?.welcomeButton?.text = buttonLabel
//         // binding?.welcomeButton?.setOnClickListener(action)
//         return view
//     }

//     fun setArgs(title: String) {
//         this.title = title
//         // this.buttonLabel = label
//         // this.action = action
//         binding?.title?.text = title
//         // binding?.welcomeButton?.text = label
//         // binding?.welcomeButton?.setOnClickListener(action)
//     }

//     override fun onDestroyView() {
//         binding = null
//         super.onDestroyView()
//     }

//     // companion object {
//         /**
//          * Use this factory method to create a new instance of
//          * this fragment using the provided parameters.
//          *
//          * @param param1 Parameter 1.
//          * @param param2 Parameter 2.
//          * @return A new instance of fragment DoclistItem.
//          */
//         // TODO: Rename and change types and number of parameters
//         // @JvmStatic
//         // fun newInstance(param1: String, param2: String) =
//         //     DoclistItem().apply {
//         //         arguments = Bundle().apply {
//         //             putString(ARG_PARAM1, param1)
//         //             putString(ARG_PARAM2, param2)
//         //         }
//         //     }
//     // }
// }
