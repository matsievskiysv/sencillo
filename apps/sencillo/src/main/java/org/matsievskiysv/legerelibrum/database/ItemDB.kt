package org.matsievskiysv.sencillo

import android.content.Context

import androidx.room.*

import kotlinx.coroutines.flow.Flow

private const val TABLE_DOCS = "documents"

@Entity(tableName = TABLE_DOCS)
class DocumentItem(
    @ColumnInfo(name = "uri")
    val uri: String,
    @PrimaryKey(name = "path")
    val path: String,
    @ColumnInfo(name = "name")
    var name: String,
) {
    @ColumnInfo(name = "access_time")
    var atime: Long = 0
    @ColumnInfo(name = "page")
    var page: Int = 1
    @ColumnInfo(name = "pages")
    var pages: Int = 1
    @ColumnInfo(name = "crop", typeAffinity = ColumnInfo.BLOB)
    var crop: ByteArray = byteArrayOf()

    override fun toString(): String {
        return """DocItem[${uri}
${path}
name:${name}
atime:${atime}
page:${page}
pages:${pages}
crop:${crop.size > 0}]"""
    }

}

@Dao
interface DocumentItemDao {
    @Query("""SELECT * FROM ${TABLE_DOCS}""")
    fun getAllFlow(): Flow<List<DocumentItem>>

    @Query("""SELECT * FROM ${TABLE_DOCS} WHERE path = :path LIMIT 1""")
    fun getByPath(path: String): DocumentItem?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(item: DocumentItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItems(vararg item: DocumentItem)

    @Update
    fun updateItem(item: DocumentItem)

    @Update
    fun updateItems(vararg item: DocumentItem)

    @Delete
    fun deleteItem(item: DocumentItem)

    @Delete
    fun deleteItems(vararg item: DocumentItem)
}

@Database(entities = arrayOf(DocumentItem::class), version = 1)
abstract class DocumentDatabase : RoomDatabase() {
    abstract fun documentItemDao(): DocumentItemDao
}

object DocDatabaseBuilder {

    private var INSTANCE: DocumentDatabase? = null

    fun getInstance(context: Context): DocumentDatabase {
        if (INSTANCE == null) {
            synchronized(DocumentDatabase::class) {
                INSTANCE = buildRoomDB(context)
            }
        }
        return INSTANCE!!
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            DocumentDatabase::class.java,
            TABLE_DOCS).build()
}


private const val TABLE_UI = "ui"

@Entity(tableName = TABLE_UI)
class UiItem(
    @ColumnInfo(name = "uri")
    val uri: String,
    @PrimaryKey(name = "path")
    val path: String,
) {
    @ColumnInfo(name = "top_to_bottom")
    var topToBottom: Boolean = true
    @ColumnInfo(name = "left_to_right")
    var leftToRight: Boolean = true

    override fun toString(): String {
        return """UiItem[${uri}
${path}
topToBottom:${topToBottom}
leftToRight:${leftToRight}]"""
    }
}

@Dao
interface UiItemDao {
    @Query("""SELECT * FROM ${TABLE_UI}""")
    fun getAll(): Flow<List<UiItem>>

    @Query("""SELECT * FROM ${TABLE_UI} WHERE path = :path LIMIT 1""")
    fun getByPath(path: String): UiItem?

    @Query("""SELECT * FROM ${TABLE_UI} WHERE path = :path LIMIT 1""")
    fun getByPathFlow(path: String): Flow<UiItem?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(item: UiItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItems(vararg item: UiItem)

    @Update
    fun updateItem(item: UiItem)

    @Update
    fun updateItems(vararg item: UiItem)

    @Delete
    fun deleteItem(item: UiItem)

    @Delete
    fun deleteItems(vararg item: UiItem)
}

@Database(entities = arrayOf(UiItem::class), version = 1)
abstract class UiDatabase : RoomDatabase() {
    abstract fun uiItemDao(): UiItemDao
}

object UiDatabaseBuilder {

    private var INSTANCE: UiDatabase? = null

    fun getInstance(context: Context): UiDatabase {
        if (INSTANCE == null) {
            synchronized(UiDatabase::class) {
                INSTANCE = buildRoomDB(context)
            }
        }
        return INSTANCE!!
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            UiDatabase::class.java,
            TABLE_UI
        ).build()
}
