package org.matsievskiysv.sencillo

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat

import org.matsievskiysv.sencillo.databinding.SettingsBinding

class GlobalSettingsActivity: AppCompatActivity() {
    private lateinit var binding: SettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SettingsBinding.inflate(layoutInflater)
        setSupportActionBar(binding.globalSettingsToolbar)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.global_settings_placeholder, GlobalSettingsFragment())
            .commit()

        binding.globalSettingsToolbar.setNavigationOnClickListener {
            finish()
        }

        setContentView(binding.root)
    }

    class GlobalSettingsFragment() : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.global_preferences, rootKey)
        }
    }
}
